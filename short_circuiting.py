is_friend = True
is_user = True


print(is_friend and is_user)

'''
When we use 'and', the interpreter will check all of the conditions inside the if statement.
When we use 'or', the interpreter will check one : one, whether the first is equal True, then 
the others conditions are unnecessary
'''

if is_friend and is_user:
    print('best friends forever')