# Is an order collection the uniques objects

# Removing duplicates
my_list = [1,2,3,4,5,5,5]
my_list = set(my_list)
print(f'my_list: {my_list}')

my_set = {1,2,3,4,5,5,5}
my_set.add(100)
my_set.add(2)
print(f'my_set: {my_set}')
print(3 in my_set)

# Methods
set1 = {1,2,3,4,5}
set2 = {4,5,6,7,8,9,10}

print(f'difference set1: {set1.difference(set2)}')
print(f'difference set2: {set2.difference(set1)}')
set1.discard(5)
print(f'discard: {set1}')

