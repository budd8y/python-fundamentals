basket = [1,2,3,4,5]

print(basket)

# adding
basket.append(6) # in the final
basket.insert(0, 200) # where you specifing 
basket.extend([400,441]) # union 

print(f'After adding block: {basket}')

# removing
basket.pop() # in the final
basket.pop(0) # in according with the index 
basket.remove(4) # like pop with index
print(f'After removing block: {basket}')

# clear
basket.clear()
print(f'After clear block: {basket}')

# index
new_basket = ['a', 'x',  'b', 'y', 'c', 'z', 'd', 'e']
print(f'NewBasket: {new_basket}')
print(f'Index: {new_basket.index("d")}')
print('d' in new_basket)
print(f'count: {new_basket.count("d")}')

new_basket.sort()
print(f'sort: {new_basket}')

new_basket.reverse()
print(f'reverse: {new_basket}')
