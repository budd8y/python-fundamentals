# Dictionary -> like map, object in others languages 

dictionary = {
    'a': 1,
    'b': 2
}

dictionary2 = {
    'a': [1,2,3],
    'b': 'hello',
    'x': True
}

my_list = [
    {
    'a': [1,2,3],
    'b': 'hello',
    'x': True
    },
    {
    'a': [4,5,6],
    'b': 'hello',
    'x': False
    },
]

print(dictionary)
print(dictionary2)
print(my_list)

# Methods
user = {
    'name': 'JOJO',
    'email': 'jojo@jojo.com',
    'age': 20
}


user2 = dict(name='JoJo') # another way to create dictionaries, but this is unusual. 

print(user.get('age', 55)) # if don't exist age key in the dict, we are assing 55 

print('name' in user.keys())
print('JOJO' in user.values())
print(user.items())

user.clear()
print(user)

user3 = user2.copy()
print(user3)