# with list we can have differents types of data [collection of data]-> like Arrays
li  = [1,2,3,4]
li2 = ['a', 'b', 'c']
li3 = [1, 2, 'a', True]

# Data Structure


# List slicing
amazon_cart = [
    'notebooks', 
    'sunglasses',
    'toys',
    'grapes'
]
print(amazon_cart[0:2])
print(amazon_cart[0:1])
print(amazon_cart[0::2])

'''
here we have two lists that point to the same address because new_cart copy only part of the amazon_cart, 
when we change, for example new_cart, this reflected in both because it's has just one memory address
'''
amazon_cart[0] = 'laptop'
new_cart = amazon_cart
new_cart[0] = 'gum'

'''
here we can create a entire copy of the amazon_address, what's mean that now we have another list from amazon_cart.
In other words we create a new list, i.e we have a new memory address, from amazon_cart.
'''
cart = amazon_cart[:]
cart[0] = 'anothet thing'

print(f'amazon_cart: {amazon_cart}')
print(f'new_cart: {new_cart}')
print(f'cart: {cart}')

