
basket = ['a', 'x',  'b', 'y', 'c', 'z', 'd', 'e']
basket.sort()
basket.reverse()
print(f'Slicing reverse: {basket[::-1]}')
print(f'Origin list: {basket}')

new_list = list(range(1, 100))
print(new_list)

sentence = ' '
new_sentence = sentence.join(['hi', 'my', 'name', 'is', 'JOJO'])

print(f'new sentence: {new_sentence}')

# list unpacking

a,b,c, *other, d = [1,2,3,4,5,6,7,8,9]
print(a)
print(b)
print(c)
print(other)
print(d)
